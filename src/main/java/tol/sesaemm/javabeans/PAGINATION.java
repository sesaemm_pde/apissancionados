/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class PAGINATION implements Serializable
  {
    private Integer pageSize;
    private Integer page;
    private Integer totalRows;
    private Boolean hasNextPage;

    public Integer getPageSize() {
        return pageSize;
      }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
      }

    public Integer getPage() {
        return page;
      }

    public void setPage(Integer page) {
        this.page = page;
      }

    public Integer getTotalRows() {
        return totalRows;
      }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
      }

    public Boolean getHasNextPage() {
        return hasNextPage;
      }

    public void setHasNextPage(Boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
      }

  }
