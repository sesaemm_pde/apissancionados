/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList; 
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.javabeans.DOCUMENTO;
import tol.sesaemm.javabeans.INHABILITACION;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SSANCIONADOS
  {

    @BsonId
    @BsonProperty("_id")  
    @JsonIgnore                                                                 
    @Exclude                                                                    
    private ObjectId idSpdn;
    private String id;
    private String fechaCaptura;
    private String expediente;
    private INSTITUCION_DEPENDENCIA institucionDependencia;
    private SERVIDOR_PUBLICO_SANCIONADO servidorPublicoSancionado;
    private String autoridadSancionadora;
    private TIPO_FALTA tipoFalta;
    private ArrayList<TIPO_SANCION_SER> tipoSancion;
    private String causaMotivoHechos;
    private RESOLUCION_SER resolucion;
    private MULTA_SSANCIONADOS multa;
    private INHABILITACION inhabilitacion;
    private String observaciones;
    private ArrayList<DOCUMENTO> documentos;
    @JsonIgnore                                                                 
    private String publicar;
    @JsonIgnore                                                                 
    private METADATOS metadatos;
    private METADATA metadata;
    @JsonIgnore                                                                 
    private String dependencia;

    public SSANCIONADOS()
      {
      }

    public ObjectId getIdSpdn()
      {
        return idSpdn;
      }

    public void setIdSpdn(ObjectId idSpdn)
      {
        this.idSpdn = idSpdn;
      }
    
    public String getId()
      {
        return id;
      }

    public void setId(String id)
      {
        this.id = id;
      }

    public String getFechaCaptura()
      {
        return fechaCaptura;
      }

    public void setFechaCaptura(String fechaCaptura)
      {
        this.fechaCaptura = fechaCaptura;
      }

    public String getExpediente()
      {
        return expediente;
      }

    public void setExpediente(String expediente)
      {
        this.expediente = expediente;
      }

    public INSTITUCION_DEPENDENCIA getInstitucionDependencia()
      {
        return institucionDependencia;
      }

    public void setInstitucionDependencia(INSTITUCION_DEPENDENCIA institucionDependencia)
      {
        this.institucionDependencia = institucionDependencia;
      }

    public SERVIDOR_PUBLICO_SANCIONADO getServidorPublicoSancionado()
      {
        return servidorPublicoSancionado;
      }

    public void setServidorPublicoSancionado(SERVIDOR_PUBLICO_SANCIONADO servidorPublicoSancionado)
      {
        this.servidorPublicoSancionado = servidorPublicoSancionado;
      }

    public String getAutoridadSancionadora()
      {
        return autoridadSancionadora;
      }

    public void setAutoridadSancionadora(String autoridadSancionadora)
      {
        this.autoridadSancionadora = autoridadSancionadora;
      }

    public TIPO_FALTA getTipoFalta()
      {
        return tipoFalta;
      }

    public void setTipoFalta(TIPO_FALTA tipoFalta)
      {
        this.tipoFalta = tipoFalta;
      }

    public ArrayList<TIPO_SANCION_SER> getTipoSancion()
      {
        return tipoSancion;
      }

    public void setTipoSancion(ArrayList<TIPO_SANCION_SER> tipoSancion)
      {
        this.tipoSancion = tipoSancion;
      }

    public String getCausaMotivoHechos()
      {
        return causaMotivoHechos;
      }

    public void setCausaMotivoHechos(String causaMotivoHechos)
      {
        this.causaMotivoHechos = causaMotivoHechos;
      }

    public RESOLUCION_SER getResolucion()
      {
        return resolucion;
      }

    public void setResolucion(RESOLUCION_SER resolucion)
      {
        this.resolucion = resolucion;
      }

    public MULTA_SSANCIONADOS getMulta()
      {
        return multa;
      }

    public void setMulta(MULTA_SSANCIONADOS multa)
      {
        this.multa = multa;
      }

    public INHABILITACION getInhabilitacion()
      {
        return inhabilitacion;
      }

    public void setInhabilitacion(INHABILITACION inhabilitacion)
      {
        this.inhabilitacion = inhabilitacion;
      }

    public String getObservaciones()
      {
        return observaciones;
      }

    public void setObservaciones(String observaciones)
      {
        this.observaciones = observaciones;
      }

    public ArrayList<DOCUMENTO> getDocumentos()
      {
        return documentos;
      }

    public void setDocumentos(ArrayList<DOCUMENTO> documentos)
      {
        this.documentos = documentos;
      }

    public String getPublicar()
      {
        return publicar;
      }

    public void setPublicar(String publicar)
      {
        this.publicar = publicar;
      }

    public METADATOS getMetadatos()
      {
        return metadatos;
      }

    public void setMetadatos(METADATOS metadatos)
      {
        this.metadatos = metadatos;
      }

    public METADATA getMetadata()
      {
        return metadata;
      }

    public void setMetadata(METADATA metadata)
      {
        this.metadata = metadata;
      }

    public String getDependencia()
      {
        return dependencia;
      }

    public void setDependencia(String dependencia)
      {
        this.dependencia = dependencia;
      }
    
  }