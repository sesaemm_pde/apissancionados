/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.PUESTO;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class SERVIDOR_PUBLICO_SANCIONADO {
    
    private String rfc;
    private String curp;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private GENERO genero;
    private String puesto;
    @BsonIgnore
    @JsonIgnore                                                                 // anotacion para jackson
    @Exclude                                                                    // anotacion personalizada para gson
    private PUESTO puestoCompleto;
    private String nivel;

    public String getRfc()
      {
        return rfc;
      }

    public void setRfc(String rfc)
      {
        this.rfc = rfc;
      }

    public String getCurp()
      {
        return curp;
      }

    public void setCurp(String curp)
      {
        this.curp = curp;
      }

    public String getNombres()
      {
        return nombres;
      }

    public void setNombres(String nombres)
      {
        this.nombres = nombres;
      }

    public String getPrimerApellido()
      {
        return primerApellido;
      }

    public void setPrimerApellido(String primerApellido)
      {
        this.primerApellido = primerApellido;
      }

    public String getSegundoApellido()
      {
        return segundoApellido;
      }

    public void setSegundoApellido(String segundoApellido)
      {
        this.segundoApellido = segundoApellido;
      }

    public GENERO getGenero()
      {
        return genero;
      }

    public void setGenero(GENERO genero)
      {
        this.genero = genero;
      }

    public String getPuesto()
      {
        return puesto;
      }

    public void setPuesto(String puesto)
      {
        this.puesto = puesto;
      }

    public PUESTO getPuestoCompleto()
      {
        return puestoCompleto;
      }

    public void setPuestoCompleto(PUESTO puestoCompleto)
      {
        this.puestoCompleto = puestoCompleto;
      }

    public String getNivel()
      {
        return nivel;
      }

    public void setNivel(String nivel)
      {
        this.nivel = nivel;
      }

}
