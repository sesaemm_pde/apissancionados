/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class TIPO_FALTA implements Comparable<TIPO_FALTA>
  {
    
    @BsonIgnore
    @JsonIgnore                                                                 
    @Exclude                                                                    
    private ObjectId _id;
    private String clave;
    private String valor;
    private String descripcion;
    @BsonIgnore
    @JsonIgnore                                                                 
    @Exclude                                                                    
    private int contador;

    public TIPO_FALTA()
      {
        this.clave = null;
        this.valor = null;
        this.descripcion = null;
      }
    

    public ObjectId getId()
      {
        return _id;
      }

    public void setId(ObjectId _id)
      {
        this._id = _id;
      }

    public String getClave()
      {
        return clave;
      }

    public void setClave(String clave)
      {
        this.clave = clave;
      }

    public String getValor()
      {
        return valor;
      }

    public void setValor(String valor)
      {
        this.valor = valor;
      }

    public String getDescripcion()
      {
        return descripcion;
      }

    public void setDescripcion(String descripcion)
      {
        this.descripcion = descripcion;
      }

    public int getContador()
      {
        return contador;
      }

    public void setContador(int contador)
      {
        this.contador = contador;
      }

    @Override
    public String toString()
      {
        return this.valor+""+this.clave;
      }
    
    @Override
    public int compareTo(TIPO_FALTA o)
      {
        return this.valor.compareTo(o.getValor());
      }
    
  }
