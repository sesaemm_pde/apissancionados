/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class FILTRO_SSANCIONADOS_WS {
    
    private int pageSize; 
    private int page;    
    private FILTRO_SSANCIONADOS_SORT sort;
    private FILTRO_SSANCIONADOS_QUERY query;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public FILTRO_SSANCIONADOS_SORT getSort() {
        return sort;
    }

    public void setSort(FILTRO_SSANCIONADOS_SORT sort) {
        this.sort = sort;
    }

    public FILTRO_SSANCIONADOS_QUERY getQuery() {
        return query;
    }

    public void setQuery(FILTRO_SSANCIONADOS_QUERY query) {
        this.query = query;
    }
    
}
