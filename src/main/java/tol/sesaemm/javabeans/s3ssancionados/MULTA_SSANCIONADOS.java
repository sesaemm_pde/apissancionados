/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

import tol.sesaemm.javabeans.MONEDA;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class MULTA_SSANCIONADOS
  {
    private Double monto;
    private MONEDA moneda;

    public Double getMonto()
      {
        return monto;
      }

    public void setMonto(Double monto)
      {
        this.monto = monto;
      }

    public MONEDA getMoneda()
      {
        return moneda;
      }

    public void setMoneda(MONEDA moneda)
      {
        this.moneda = moneda;
      }

  }
