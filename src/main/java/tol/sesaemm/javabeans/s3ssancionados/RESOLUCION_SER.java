/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s3ssancionados;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class RESOLUCION_SER
  {
    
    private String url;
    private String fechaResolucion;

    public String getUrl()
      {
        return url;
      }

    public void setUrl(String url)
      {
        this.url = url;
      }

    public String getFechaResolucion()
      {
        return fechaResolucion;
      }

    public void setFechaResolucion(String fechaResolucion)
      {
        this.fechaResolucion = fechaResolucion;
      }

  }
