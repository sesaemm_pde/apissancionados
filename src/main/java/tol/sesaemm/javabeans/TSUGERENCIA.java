/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class TSUGERENCIA implements Serializable
  {
    @BsonId
    @BsonProperty("_id")
    private ObjectId id;
    private String sugerencia;
    private String fecha;
    private boolean leida;

    public TSUGERENCIA()
      {
      }

    public ObjectId getId()
      {
        return id;
      }

    public void setId(ObjectId id)
      {
        this.id = id;
      }

    public String getSugerencia()
      {
        return sugerencia;
      }

    public void setSugerencia(String sugerencia)
      {
        this.sugerencia = sugerencia;
      }

    public String getFecha()
      {
        return fecha;
      }

    public void setFecha(String fecha)
      {
        this.fecha = fecha;
      }

    public boolean isLeida()
      {
        return leida;
      }

    public void setLeida(boolean leida)
      {
        this.leida = leida;
      }
    
  }
