/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class GENERO
  {
    private String clave;
    private String valor;

    public GENERO()
      {
      }

    public GENERO(
            @BsonProperty("clave") 
            String clave, 
            @BsonProperty("valor") 
            String valor
    )
      {
        this.clave = clave;
        this.valor = valor;
      }

    public String getClave()
      {
        return clave;
      }

    public void setClave(String clave)
      {
        this.clave = clave;
      }

    public String getValor()
      {
        return valor;
      }

    public void setValor(String valor)
      {
        this.valor = valor;
      }
    
  }
