package tol.sesaemm.ing.cvlg.controlador;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@RestController
public class ControladorServicioRest
{
    /**
     * Mensaje principale del web service
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String webapp()
    {
        return "Servicio Web de conexión SESAEMM - SESNA";
    }

    /**
     * Obtiene en formato Json estructura peticion Post (atributos de paginacion
     * y resultados de consulta) de servidores sancionados
     *
     * @param Bcuerpo Estructura Post
     * @return Cadena de estructura Post en formato Json
     * @throws java.lang.Exception
     */
    @RequestMapping(value = "/v1/ssancionados", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String obtenerEstructuraPostFormatoJsonServidoresSancionados(@RequestBody(required = false) String Bcuerpo) throws Exception
    { 
        LogicaDeNegocio logicaNegocio;                          
        FILTRO_SSANCIONADOS_WS servidoresSancionadosPost;       
        SSANCIONADOSROOT servidoresSancionados;                 
        SSANCIONADOS camposPublicos;                            
        Document documentoDeError;                              

        logicaNegocio = new LogicaDeNegocioV01();

        try
        {

            Bcuerpo = new String(Bcuerpo.getBytes("ISO-8859-1"), "UTF-8");
            servidoresSancionadosPost = new Gson().fromJson(Bcuerpo, FILTRO_SSANCIONADOS_WS.class);
            servidoresSancionadosPost = logicaNegocio.obtenerValoresPorDefaultFiltroServidoresSancionados(servidoresSancionadosPost);

            if (logicaNegocio.esValidoRFC(servidoresSancionadosPost.getQuery().getRfcSolicitante()) || servidoresSancionadosPost.getQuery().getRfcSolicitante() == null || servidoresSancionadosPost.getQuery().getRfcSolicitante().isEmpty())
            { 

                servidoresSancionados = logicaNegocio.obtenerServidoresPublicosSancionadosPost(servidoresSancionadosPost);
                camposPublicos = logicaNegocio.obtenerEstatusDePrivacidadCamposServidoresSancionados();
                logicaNegocio.guardarBitacoraDeEventos(servidoresSancionadosPost, servidoresSancionados.getPagination(), "Correcta");

                return logicaNegocio.generarJsonServidoresSancionados(servidoresSancionados, camposPublicos, servidoresSancionadosPost.getQuery().getRfcSolicitante());
            } 
            else
            { 

                documentoDeError = new Document();
                documentoDeError.append("code", "ssancionados_p02");
                documentoDeError.append("message", "Error el RFC no es válido. ");

                logicaNegocio.guardarBitacoraDeEventos(servidoresSancionadosPost, null, "El RFC no es válido");

                return documentoDeError.toJson();
            } 

        }
        catch (Exception ex)
        { 

            documentoDeError = new Document();
            documentoDeError.append("code", "ssancionados_p01");
            documentoDeError.append("message", "Error al obtener listado de servidores públicos sancionados: " + ex.toString());

            servidoresSancionadosPost = logicaNegocio.obtenerValoresPorDefaultFiltroServidoresSancionados(new FILTRO_SSANCIONADOS_WS());
            logicaNegocio.guardarBitacoraDeEventos(servidoresSancionadosPost, null, "Error al obtener listado de servidores públicos sancionados: " + ex.toString());

            return documentoDeError.toJson();
        } 
    } 

    /**
     * Obtiene dependecias en formato Json de servidores sancionados
     *
     * @return Cadena de dependencias en formato Json
     */
    @RequestMapping(value = "/v1/ssancionados/dependencias", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String obtenerDependenciasServidoresSancionados()
    { 

        LogicaDeNegocio logicaNegocio;              
        Document documentoDeError;                  

        try
        {
            logicaNegocio = new LogicaDeNegocioV01();
            return logicaNegocio.generarJsonDependenciasServidoresSancionados(logicaNegocio.obtenerDependenciasServidoresSancionados());
        }
        catch (Exception ex)
        { 

            documentoDeError = new Document();
            documentoDeError.append("code", "Ssancionados_p01");
            documentoDeError.append("message", "Error al obtener listado de Dependencias: " + ex.toString());
            return documentoDeError.toJson();
        } 
    } 

    @RequestMapping(value = "/403", produces = "application/json;charset=UTF-8")
    public String error403()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "403");
        documentoDeError.append("message", "Forbidden");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/404", produces = "application/json;charset=UTF-8")
    public String error404()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "404");
        documentoDeError.append("message", "Not Found");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/405", produces = "application/json;charset=UTF-8")
    public String error405()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "405");
        documentoDeError.append("message", "Method Not Allowed");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/500", produces = "application/json;charset=UTF-8")
    public String error500()
    {
        Document documentoDeError = new Document();

        documentoDeError.append("code", "500");
        documentoDeError.append("message", "Internal Server Error");

        return documentoDeError.toJson();
    }
    
}