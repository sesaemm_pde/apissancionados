/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.Gson;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DAOWs
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost Estructura para la peticion POST
     * @param strEstatusConsulta Estatus de la consulta
     * @param valoresDePaginacion Valores de paginacion
     *
     * @throws Exception
     */
    public static void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                      
        MongoDatabase database;                                             
        MongoCollection<Document> coleccion;                                
        FILTRO_SSANCIONADOS_WS estructuraPostSSancionados;                  
        PAGINATION valoresDePaginacionSSancionados;                         
        ArrayList<Document> arregloListaFiltrosDeBusqueda;                  
        Document documentoBitacora;                                         
        Date fecha;                                                         
        SimpleDateFormat formateador;                                       
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        fecha = new Date();
        formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                coleccion = database.getCollection("bitacora_servicios_web");

                arregloListaFiltrosDeBusqueda = new ArrayList<>();
                documentoBitacora = new Document();

                
                if (estructuraPost instanceof FILTRO_SSANCIONADOS_WS)
                { 

                    estructuraPostSSancionados = (FILTRO_SSANCIONADOS_WS) estructuraPost;
                    
                    if (estructuraPostSSancionados.getQuery().getId() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "id")
                                .append("valor", estructuraPostSSancionados.getQuery().getId())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getNombres() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "nombres")
                                .append("valor", estructuraPostSSancionados.getQuery().getNombres())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getPrimerApellido() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "primerApellido")
                                .append("valor", estructuraPostSSancionados.getQuery().getPrimerApellido())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getSegundoApellido() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "segundoApellido")
                                .append("valor", estructuraPostSSancionados.getQuery().getSegundoApellido())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getRfc() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "rfc")
                                .append("valor", estructuraPostSSancionados.getQuery().getRfc())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getCurp() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "curp")
                                .append("valor", estructuraPostSSancionados.getQuery().getCurp())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getInstitucionDependencia() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "institucionDependencia")
                                .append("valor", estructuraPostSSancionados.getQuery().getInstitucionDependencia())
                        );
                    }

                    if (estructuraPostSSancionados.getQuery().getTipoSancion() != null)
                    {
                        arregloListaFiltrosDeBusqueda.add(new Document("filtro", "tipoSancion")
                                .append("valor", estructuraPostSSancionados.getQuery().getTipoSancion())
                        );
                    }
                    
                    if (estructuraPostSSancionados.getQuery().getRfcSolicitante() != null && !estructuraPostSSancionados.getQuery().getRfcSolicitante().isEmpty())
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", estructuraPostSSancionados.getQuery().getRfcSolicitante())
                                .append("privacidadConsulta", "PRV")
                                .append("sistemaConsultado", "Servidores Sancionados")
                        );
                    } 
                    else
                    { 
                        documentoBitacora.append("datosGeneralesDeConsulta", new Document()
                                .append("fechaConsulta", formateador.format(fecha))
                                .append("rfcSolicitante", (estructuraPostSSancionados.getQuery().getRfcSolicitante() == null) ? "" : estructuraPostSSancionados.getQuery().getRfcSolicitante())
                                .append("privacidadConsulta", "PUB")
                                .append("sistemaConsultado", "Servidores Sancionados")
                        );
                    } 
                    
                    if (valoresDePaginacion != null)
                    { 

                        valoresDePaginacionSSancionados = (PAGINATION) valoresDePaginacion;

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", valoresDePaginacionSSancionados.getTotalRows())
                        );
                    } 
                    else
                    { 

                        documentoBitacora.append("resultadosDeConsulta", new Document()
                                .append("filtrosDeBusqueda", arregloListaFiltrosDeBusqueda)
                                .append("estatusConsulta", strEstatusConsulta)
                                .append("numeroDeRegistrosConsultados", 0)
                        );
                    } 
                    
                } 
                coleccion.insertOne(documentoBitacora);
                
            }  
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error en bitacora de eventos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

    } 

    /**
     * Obtener Listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    public static ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    {

        ArrayList<USUARIO_TOKEN_ACCESO> arregloUsuariosTokenAcceso;             
        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<Document> query;                                              
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios_token_acceso");
                query = new ArrayList<>();
                query.add(new Document("$match", new Document("activo", new Document("$eq", 1))));
                arregloUsuariosTokenAcceso = collection.aggregate(query, USUARIO_TOKEN_ACCESO.class).collation(collation).into(new ArrayList<>());
               
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener usuarios token acceso: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return arregloUsuariosTokenAcceso;
    }

    
    /**
     * Obtener listado de servidores publicos sancionados
     *
     * @param servidoresSancionadosPost Servidores sancionados
     *
     * @return Listado de servidores sancionados
     *
     * @throws Exception
     */
    public static SSANCIONADOSROOT obtenerServidoresPublicosSancionadosPost(FILTRO_SSANCIONADOS_WS servidoresSancionadosPost) throws Exception
    { 

        LogicaDeNegocio logicaNegocio = new LogicaDeNegocioV01();                       
        ArrayList<SSANCIONADOS> listaServidoresPublicosSancionados;                     
        PAGINATION atributosDePaginacion;                                               
        SSANCIONADOSROOT servidoresSancionados;                                         
        MongoClient conectarBaseDatos = null;                                                  
        MongoDatabase database;                                                         
        MongoCollection<Document> collection;                                           
        AggregateIterable<Document> iteradorDeResultados;                               
        MongoCursor<Document> cursorDeResultados = null;                                       
        Document where = new Document();                                                
        Document order = new Document();                                                
        int intNumeroDocumentosEncontrados = 0;                                         
        int intNumeroSaltos = 0;                                                        
        boolean hasNextPage = false;                                                    
        Collation collation;                                                            
        ArrayList<Document> arregloTipoSancionClave = new ArrayList<>();                
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("ssancionados");
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                
                if (servidoresSancionadosPost.getQuery().getId() != null && !servidoresSancionadosPost.getQuery().getId().isEmpty())
                { 
                    where.append("identificador", servidoresSancionadosPost.getQuery().getId());
                } 
                else
                { 
                    if (servidoresSancionadosPost.getQuery().getNombres() != null && !servidoresSancionadosPost.getQuery().getNombres().isEmpty())
                    { 
                        where.append("servidorPublicoSancionado.nombres",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getNombres())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getPrimerApellido() != null && !servidoresSancionadosPost.getQuery().getPrimerApellido().isEmpty())
                    { 
                        where.append("servidorPublicoSancionado.primerApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getPrimerApellido())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getSegundoApellido() != null && !servidoresSancionadosPost.getQuery().getSegundoApellido().isEmpty())
                    { 
                        where.append("servidorPublicoSancionado.segundoApellido",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getSegundoApellido())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getRfc() != null && !servidoresSancionadosPost.getQuery().getRfc().isEmpty())
                    { 
                        where.append("servidorPublicoSancionado.rfc",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getRfc())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getCurp() != null && !servidoresSancionadosPost.getQuery().getCurp().isEmpty())
                    { 
                        where.append("servidorPublicoSancionado.curp",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getCurp())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getInstitucionDependencia() != null && !servidoresSancionadosPost.getQuery().getInstitucionDependencia().isEmpty())
                    { 
                        where.append("institucionDependencia.nombre",
                                new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getInstitucionDependencia())).append("$options", "i"));
                    } 

                    if (servidoresSancionadosPost.getQuery().getTipoSancion() != null && !servidoresSancionadosPost.getQuery().getTipoSancion().isEmpty())
                    { 

                        for (int j = 0; j < servidoresSancionadosPost.getQuery().getTipoSancion().size(); j++)
                        { 
                            arregloTipoSancionClave.add(new Document("tipoSancion.clave", new Document("$regex", logicaNegocio.crearExpresionRegularDePalabra(servidoresSancionadosPost.getQuery().getTipoSancion().get(j))).append("$options", "i")));
                        } 

                        where.append("$or", arregloTipoSancionClave);
                    } 

                    if (servidoresSancionadosPost.getQuery().getRfcSolicitante().equals("") || servidoresSancionadosPost.getQuery().getRfcSolicitante() == null)
                    { 
                        where.append("tipoFalta.clave", new Document("$eq", "AG"));
                    } 
                } 
                
                where.append("publicar", new Document("$ne", "0"));
                
                if (servidoresSancionadosPost.getSort().getRfc() != null && !servidoresSancionadosPost.getSort().getRfc().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getRfc().toLowerCase().equals("desc"))
                    {
                        order.append("data.servidorPublicoSancionado.rfc", -1);
                    }
                    else if(servidoresSancionadosPost.getSort().getRfc().toLowerCase().equals("asc"))
                    {
                        order.append("data.servidorPublicoSancionado.rfc", 1);
                    }
                } 

                if (servidoresSancionadosPost.getSort().getCurp() != null && !servidoresSancionadosPost.getSort().getCurp().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getCurp().toLowerCase().equals("desc"))
                    {
                    order.append("data.servidorPublicoSancionado.curp", -1);                        
                    }
                    else if(servidoresSancionadosPost.getSort().getCurp().toLowerCase().equals("asc"))
                    {
                        order.append("data.servidorPublicoSancionado.curp", 1);
                    }
                } 

                if (servidoresSancionadosPost.getSort().getNombres() != null && !servidoresSancionadosPost.getSort().getNombres().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getNombres().toLowerCase().equals("desc"))
                    {
                        order.append("data.servidorPublicoSancionado.nombres", -1);
                    }
                    else if(servidoresSancionadosPost.getSort().getNombres().toLowerCase().equals("asc"))
                    {
                        order.append("data.servidorPublicoSancionado.nombres", 1);
                    }
                } 

                if (servidoresSancionadosPost.getSort().getPrimerApellido() != null && !servidoresSancionadosPost.getSort().getPrimerApellido().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getPrimerApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.servidorPublicoSancionado.primerApellido", -1);
                    }
                    else if(servidoresSancionadosPost.getSort().getPrimerApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.servidorPublicoSancionado.primerApellido", 1);                        
                    }
                } 

                if (servidoresSancionadosPost.getSort().getSegundoApellido() != null && !servidoresSancionadosPost.getSort().getSegundoApellido().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getSegundoApellido().toLowerCase().equals("desc"))
                    {
                        order.append("data.servidorPublicoSancionado.segundoApellido", -1);
                    }
                    else if(servidoresSancionadosPost.getSort().getSegundoApellido().toLowerCase().equals("asc"))
                    {
                        order.append("data.servidorPublicoSancionado.segundoApellido", 1);                        
                    }
                } 

                if (servidoresSancionadosPost.getSort().getInstitucionDependencia() != null && !servidoresSancionadosPost.getSort().getInstitucionDependencia().isEmpty())
                { 
                    if(servidoresSancionadosPost.getSort().getInstitucionDependencia().toLowerCase().equals("desc"))
                    {
                        order.append("data.institucionDependencia.nombre", -1);
                    }
                    else if(servidoresSancionadosPost.getSort().getInstitucionDependencia().toLowerCase().equals("asc"))
                    {
                        order.append("data.institucionDependencia.nombre", 1);                        
                    }
                } 

                if (order.isEmpty())
                { 
                    order.append("data.fechaCaptura", 1);
                } 
                
                iteradorDeResultados = collection.aggregate(
                        Arrays.asList(
                                new Document("$addFields",
                                        new Document("fechaCaptura", "$fechaCaptura")
                                                .append("expediente", "$expediente")
                                                .append("identificador", new Document()
                                                        .append("$convert", new Document()
                                                                .append("input", "$_id")
                                                                .append("to", "string")
                                                                .append("onError", "")
                                                                .append("onNull", "")
                                                        )
                                                )
                                ),
                                new Document("$match", where),
                                new Document("$count", "totalCount")
                        )
                ).collation(collation).allowDiskUse(true);

                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    intNumeroDocumentosEncontrados = cursorDeResultados.next().getInteger("totalCount");
                } 

                if (servidoresSancionadosPost.getPage() > 1)
                { 
                    intNumeroSaltos = (int) ((servidoresSancionadosPost.getPage() - 1) * servidoresSancionadosPost.getPageSize());
                } 

                if ((servidoresSancionadosPost.getPage() * servidoresSancionadosPost.getPageSize()) < intNumeroDocumentosEncontrados)
                { 
                    hasNextPage = true;
                } 

                listaServidoresPublicosSancionados = collection.aggregate(
                        Arrays.asList(
                                new Document("$addFields",
                                        new Document("fechaCaptura", "$fechaCaptura")
                                                .append("expediente", "$expediente")
                                                .append("identificador", new Document()
                                                        .append("$convert", new Document()
                                                                .append("input", "$_id")
                                                                .append("to", "string")
                                                                .append("onError", "")
                                                                .append("onNull", "")
                                                        )
                                                )
                                ),
                                new Document("$match",
                                        where
                                ),
                                new Document("$project",
                                        new Document("data", "$$ROOT")
                                ),
                                new Document("$sort",
                                        order
                                ),
                                new Document("$skip", intNumeroSaltos),
                                new Document("$limit", servidoresSancionadosPost.getPageSize()),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$data")
                                )
                        ), SSANCIONADOS.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());;
                
                atributosDePaginacion = new PAGINATION();
                atributosDePaginacion.setPage(servidoresSancionadosPost.getPage());
                atributosDePaginacion.setPageSize(servidoresSancionadosPost.getPageSize());
                atributosDePaginacion.setTotalRows(intNumeroDocumentosEncontrados);
                atributosDePaginacion.setHasNextPage(hasNextPage);
                
                servidoresSancionados = new SSANCIONADOSROOT();
                servidoresSancionados.setPagination(atributosDePaginacion);
                servidoresSancionados.setResults(listaServidoresPublicosSancionados);
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener los servidores públicos sancionados: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return servidoresSancionados;
    } 

    /**
     * Obtener estatus de privacidad campos servidores sancionados
     *
     * @param coleccion Coleccion de datos
     *
     * @return Estatus de privacidad campos servidores sancionados
     *
     * @throws Exception
     */
    public static SSANCIONADOS obtenerEstatusDePrivacidadCamposServidoresSancionados(String coleccion) throws Exception
    { 

        SSANCIONADOS estatusDePrivacidad;                           
        MongoClient conectarBaseDatos = null;                              
        MongoDatabase database;                                     
        MongoCollection<Document> collection;                       
        Document documentoCamposPublicosSSANCIONADOS;               
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            estatusDePrivacidad = new SSANCIONADOS();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("campos_publicos");

                documentoCamposPublicosSSANCIONADOS = collection.find(new Document("coleccion", coleccion)).first();
                if (documentoCamposPublicosSSANCIONADOS != null)
                { 
                    estatusDePrivacidad = new Gson().fromJson(documentoCamposPublicosSSANCIONADOS.toJson(), SSANCIONADOS.class);
                } 
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return estatusDePrivacidad;
    } 

    /**
     * Obtener dependencias servidores sancionados
     *
     * @return Dependencias servidores sancionados
     *
     * @throws Exception
     */
    public static ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresSancionados() throws Exception
    { 

        ArrayList<INSTITUCION_DEPENDENCIA> listaInstitucionDependencia;                         
        MongoClient conectarBaseDatos = null;                                                          
        MongoDatabase database;                                                                 
        MongoCollection<Document> collection;                                                   
        Collation collation;                                                                    
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();
            listaInstitucionDependencia = new ArrayList<>();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("ssancionados");
                collation = Collation.builder()
                        .locale("es")
                        .caseLevel(true)
                        .build();
                
                listaInstitucionDependencia = collection.aggregate(
                        Arrays.asList(
                                new Document("$project", new Document("nombre", "$institucionDependencia.nombre")
                                        .append("siglas", "$institucionDependencia.siglas")
                                        .append("clave", "$institucionDependencia.clave")
                                ),
                                new Document("$group",
                                        new Document("_id", new Document("nombre", "$nombre")
                                                .append("siglas", "$siglas")
                                                .append("clave", "$clave"))
                                ),
                                new Document("$sort", new Document("_id.nombre", 1)
                                ),
                                new Document("$replaceRoot",
                                        new Document("newRoot", "$_id")
                                )
                        ), INSTITUCION_DEPENDENCIA.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener las Dependencias : " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaInstitucionDependencia;
    } 
    

    /**
     * Verifica si el RFC es valido
     *
     * @param rfc RFC a validar
     *
     * @return Verdadero o falso
     *
     * @throws Exception
     */
    public static boolean esValidoRFC(String rfc) throws Exception
    { 

        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> iteradorDeResultados;                            
        MongoCursor<Document> cursorDeResultados = null;                               
        boolean esValido = false;                                               
        ConfVariablesEjecucion configuracionVariables;
            
        configuracionVariables = new ConfVariablesEjecucion();
        
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(configuracionVariables.getBD());
                collection = database.getCollection("usuarios");
                iteradorDeResultados = collection.find(new Document("rfc", rfc));
                cursorDeResultados = iteradorDeResultados.iterator();
                if (cursorDeResultados.hasNext())
                { 
                    esValido = true;
                } 
                
            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        { 
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        } 
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursorDeResultados);
        }

        return esValido;
    } 

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 
        
        if(cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 
        
        if(conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 
        
    } 

}
