/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaNegocio;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.PAGINATION;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_QUERY;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_SORT;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */

public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost      Estructura para la peticion POST
     * @param strEstatusConsulta  Estatus de la consulta
     * @param valoresDePaginacion Valores de paginacion
     *
     * @throws Exception
     */
    @Override
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    {
        DAOWs.guardarBitacoraDeEventos(estructuraPost, valoresDePaginacion, strEstatusConsulta);
    }

    /**
     * Obtener Listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    @Override
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    {
        return DAOWs.obtenerUsuariosTokenDeAcceso();
    }

    /**
     * Obtener valores por default de la estructura post
     *
     * @param estructuraPost Estructura para la peticion POST
     *
     * @return Valores por default de la estructura post
     *
     * @throws Exception
     */
    @Override
    public FILTRO_SSANCIONADOS_WS obtenerValoresPorDefaultFiltroServidoresSancionados(FILTRO_SSANCIONADOS_WS estructuraPost) throws Exception
    {

        if (estructuraPost.getPage() <= 0)
        {
            estructuraPost.setPage(1);
        }
        if (estructuraPost.getPageSize() <= 0)
        {
            estructuraPost.setPageSize(10);
        }
        else if (estructuraPost.getPageSize() > 200)
        {
            estructuraPost.setPageSize(200);
        }
        
        if (estructuraPost.getSort() == null)
        { 
            FILTRO_SSANCIONADOS_SORT servidoresSancionadosSort = new FILTRO_SSANCIONADOS_SORT();
            
            servidoresSancionadosSort.setRfc("");
            servidoresSancionadosSort.setCurp("");
            servidoresSancionadosSort.setNombres("");
            servidoresSancionadosSort.setPrimerApellido("");
            servidoresSancionadosSort.setSegundoApellido("");
            servidoresSancionadosSort.setInstitucionDependencia("");
            
            estructuraPost.setSort(servidoresSancionadosSort);
        } 
        else
        { 
            
            if (estructuraPost.getSort().getRfc() == null)
            {
                estructuraPost.getSort().setRfc("");
            }
            if (estructuraPost.getSort().getCurp() == null)
            {
                estructuraPost.getSort().setCurp("");
            }
            if (estructuraPost.getSort().getNombres() == null)
            {
                estructuraPost.getSort().setNombres("");
            }
            if (estructuraPost.getSort().getPrimerApellido() == null)
            {
                estructuraPost.getSort().setPrimerApellido("");
            }
            if (estructuraPost.getSort().getSegundoApellido() == null)
            {
                estructuraPost.getSort().setSegundoApellido("");
            }
            if (estructuraPost.getSort().getInstitucionDependencia() == null)
            {
                estructuraPost.getSort().setInstitucionDependencia("");
            } 
        } 
        
        if (estructuraPost.getQuery() == null)
        { 
            FILTRO_SSANCIONADOS_QUERY servidoresSancionadosQuery = new FILTRO_SSANCIONADOS_QUERY();
            
            servidoresSancionadosQuery.setId("");
            servidoresSancionadosQuery.setNombres("");
            servidoresSancionadosQuery.setPrimerApellido("");
            servidoresSancionadosQuery.setSegundoApellido("");
            servidoresSancionadosQuery.setRfc("");
            servidoresSancionadosQuery.setCurp("");
            servidoresSancionadosQuery.setInstitucionDependencia("");
            servidoresSancionadosQuery.setTipoSancion(new ArrayList<>());
            servidoresSancionadosQuery.setRfcSolicitante("");
            
            estructuraPost.setQuery(servidoresSancionadosQuery);
        } 
        else
        { 
            if (estructuraPost.getQuery().getId() == null)
            {
                estructuraPost.getQuery().setId("");
            }
            if (estructuraPost.getQuery().getNombres() == null)
            {
                estructuraPost.getQuery().setNombres("");
            }
            if (estructuraPost.getQuery().getPrimerApellido() == null)
            {
                estructuraPost.getQuery().setPrimerApellido("");
            }
            if (estructuraPost.getQuery().getSegundoApellido() == null)
            {
                estructuraPost.getQuery().setSegundoApellido("");
            }
            if (estructuraPost.getQuery().getRfc() == null)
            {
                estructuraPost.getQuery().setRfc("");
            }
            if (estructuraPost.getQuery().getCurp() == null)
            {
                estructuraPost.getQuery().setCurp("");
            }
            if (estructuraPost.getQuery().getInstitucionDependencia() == null)
            {
                estructuraPost.getQuery().setInstitucionDependencia("");
            }
            if (estructuraPost.getQuery().getTipoSancion() == null)
            {
                estructuraPost.getQuery().setTipoSancion(new ArrayList<>());
            }
            if (estructuraPost.getQuery().getRfcSolicitante() == null)
            {
                estructuraPost.getQuery().setRfcSolicitante("");
            }
        } 
        
        return estructuraPost;
    }

    /**
     * Obtener listado de servidores sancionados
     *
     * @param estructuraPost Estructura Post
     *
     * @return Listado de servidores sancionados
     *
     * @throws Exception
     */
    @Override
    public SSANCIONADOSROOT obtenerServidoresPublicosSancionadosPost(FILTRO_SSANCIONADOS_WS estructuraPost) throws Exception
    {
        return DAOWs.obtenerServidoresPublicosSancionadosPost(estructuraPost);
    }

    /**
     * Obtener estatus de privacidad campos servidores sancionados
     *
     * @return Estatus de privacidad campos servidores sancionados
     *
     * @throws Exception
     */
    @Override
    public SSANCIONADOS obtenerEstatusDePrivacidadCamposServidoresSancionados() throws Exception
    {
        return DAOWs.obtenerEstatusDePrivacidadCamposServidoresSancionados("ssancionados");
    }

    /**
     * Generar un JSON de servidores sancionados
     *
     * @param servidoresSancionados               Lista de servidores sanionados
     * @param camposPublicosServidoresSancionados Estatus de privacidad campos
     *                                            servidores sancionads
     * @param rfcSolicitante                      RFC del solicitante
     *
     * @return Formato Json de servidores sancionados
     *
     * @throws Exception
     */
    @Override
    public String generarJsonServidoresSancionados(SSANCIONADOSROOT servidoresSancionados, SSANCIONADOS camposPublicosServidoresSancionados, String rfcSolicitante) throws Exception
    {

        Document documentoServidoresSancionados;                                            
        List<Document> listaDocumentosServidoresSancionados = new ArrayList<>();            
        PAGINATION atributosDePaginacion;                                                   
        ArrayList<SSANCIONADOS> listaServidoresSancionados;                                 
        List<Document> arregloTipoSancion;                                                  
        List<Document> documentos;                                                          

        atributosDePaginacion = servidoresSancionados.getPagination();
        listaServidoresSancionados = servidoresSancionados.getResults();

        for (SSANCIONADOS servidorSancionado : listaServidoresSancionados)
        { 

            documentoServidoresSancionados = new Document();
            documentoServidoresSancionados.append("id", servidorSancionado.getIdSpdn().toHexString());

            if (rfcSolicitante != null && !rfcSolicitante.isEmpty())
            { 
                documentoServidoresSancionados.append("fechaCaptura", servidorSancionado.getFechaCaptura());
                
                if (servidorSancionado.getExpediente() != null)
                {
                    documentoServidoresSancionados.append("expediente", servidorSancionado.getExpediente());
                }
                
                if (servidorSancionado.getInstitucionDependencia() == null)
                { 
                    documentoServidoresSancionados.append("institucionDependencia",
                            new Document()
                                    .append("nombre", null));
                } 
                else
                { 
                    Document documentoInstitucionDependencia = new Document();
                    documentoInstitucionDependencia.append("nombre", servidorSancionado.getInstitucionDependencia().getNombre());
                    if (servidorSancionado.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", servidorSancionado.getInstitucionDependencia().getSiglas());
                    }
                    if (servidorSancionado.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", servidorSancionado.getInstitucionDependencia().getClave());
                    }
                    documentoServidoresSancionados.append("institucionDependencia", documentoInstitucionDependencia);
                } 
                
                if (servidorSancionado.getServidorPublicoSancionado() == null)
                { 
                    documentoServidoresSancionados.append("servidorPublicoSancionado",
                        new Document()
                                .append("nombres", null)
                                .append("primerApellido", null)
                                .append("puesto", null));
                } 
                else
                { 
                    Document documentoServidorPublicoSancionado = new Document();
                    if (servidorSancionado.getServidorPublicoSancionado().getRfc() != null)
                    {
                        documentoServidorPublicoSancionado.append("rfc", servidorSancionado.getServidorPublicoSancionado().getRfc());
                    }
                    if (servidorSancionado.getServidorPublicoSancionado().getCurp() != null)
                    {
                        documentoServidorPublicoSancionado.append("curp", servidorSancionado.getServidorPublicoSancionado().getCurp());
                    }
                    documentoServidorPublicoSancionado.append("nombres", servidorSancionado.getServidorPublicoSancionado().getNombres());
                    documentoServidorPublicoSancionado.append("primerApellido", servidorSancionado.getServidorPublicoSancionado().getPrimerApellido());
                    if (servidorSancionado.getServidorPublicoSancionado().getSegundoApellido() != null)
                    {
                        documentoServidorPublicoSancionado.append("segundoApellido", servidorSancionado.getServidorPublicoSancionado().getSegundoApellido());
                    }
                    if (servidorSancionado.getServidorPublicoSancionado().getGenero() != null)
                    { 
                        Document documentoGenero = new Document();
                        if (servidorSancionado.getServidorPublicoSancionado().getGenero().getClave() != null)
                        {
                            documentoGenero.append("clave", servidorSancionado.getServidorPublicoSancionado().getGenero().getClave());
                        }
                        if (servidorSancionado.getServidorPublicoSancionado().getGenero().getValor() != null)
                        {
                            documentoGenero.append("valor", servidorSancionado.getServidorPublicoSancionado().getGenero().getValor());
                        }
                        if (!documentoGenero.isEmpty())
                        {
                            documentoServidorPublicoSancionado.append("genero", documentoGenero);
                        }
                    } 
                    documentoServidorPublicoSancionado.append("puesto", servidorSancionado.getServidorPublicoSancionado().getPuesto());
                    if (servidorSancionado.getServidorPublicoSancionado().getNivel() != null)
                    {
                        documentoServidorPublicoSancionado.append("nivel", servidorSancionado.getServidorPublicoSancionado().getNivel());
                    }
                    documentoServidoresSancionados.append("servidorPublicoSancionado", documentoServidorPublicoSancionado);
                } 
                
                if (servidorSancionado.getAutoridadSancionadora() != null)
                {
                    documentoServidoresSancionados.append("autoridadSancionadora", servidorSancionado.getAutoridadSancionadora());
                }
                
                if (servidorSancionado.getTipoFalta() == null)
                { 
                    documentoServidoresSancionados.append("tipoFalta",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null));
                } 
                else
                { 
                    Document documentoTipoFalta = new Document();
                    
                    documentoTipoFalta.append("clave", servidorSancionado.getTipoFalta().getClave());
                    
                    
                    documentoTipoFalta.append("valor", servidorSancionado.getTipoFalta().getValor());
                    
                    
                    if (servidorSancionado.getTipoFalta().getDescripcion() != null)
                    {
                        documentoTipoFalta.append("descripcion", servidorSancionado.getTipoFalta().getDescripcion());
                    }
                    
                    documentoServidoresSancionados.append("tipoFalta", documentoTipoFalta);
                } 
                
                if (servidorSancionado.getTipoSancion() == null)
                { 
                    documentoServidoresSancionados.append("tipoSancion",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null));
                } 
                else
                { 
                    arregloTipoSancion = new ArrayList<>();
                    for (int j = 0; j < servidorSancionado.getTipoSancion().size(); j++)
                    { 
                        Document documentoTipoSancion = new Document();
                        
                        documentoTipoSancion.append("clave", servidorSancionado.getTipoSancion().get(j).getClave());
                        
                        
                        documentoTipoSancion.append("valor", servidorSancionado.getTipoSancion().get(j).getValor());
                        
                        
                        if (servidorSancionado.getTipoSancion().get(j).getDescripcion() != null)
                        {
                            documentoTipoSancion.append("descripcion", servidorSancionado.getTipoSancion().get(j).getDescripcion());
                        }
                        
                        arregloTipoSancion.add(documentoTipoSancion);
                    } 
                    documentoServidoresSancionados.append("tipoSancion", arregloTipoSancion);
                } 
                
                documentoServidoresSancionados.append("causaMotivoHechos", servidorSancionado.getCausaMotivoHechos());
                
                if (servidorSancionado.getResolucion() != null)
                { 
                    Document documentoResolucion = new Document();
                    if (servidorSancionado.getResolucion().getUrl() != null)
                    {
                        documentoResolucion.append("url", servidorSancionado.getResolucion().getUrl());
                    }
                    documentoResolucion.append("fechaResolucion", servidorSancionado.getResolucion().getFechaResolucion());
                    documentoServidoresSancionados.append("resolucion", documentoResolucion);
                } 
                
                if (servidorSancionado.getMulta() != null)
                { 
                    Document documentoMulta = new Document();
                    documentoMulta.append("monto", servidorSancionado.getMulta().getMonto());
                    Document documentoMoneda = new Document();
                    if (servidorSancionado.getMulta().getMoneda() != null)
                    { 
                        documentoMoneda.append("clave", servidorSancionado.getMulta().getMoneda().getClave());
                        documentoMoneda.append("valor", servidorSancionado.getMulta().getMoneda().getValor());
                        documentoMulta.append("moneda", documentoMoneda);
                    } 
                    else
                    { 
                        documentoMoneda.append("clave", null);
                        documentoMoneda.append("valor", null);
                        documentoMulta.append("moneda", documentoMoneda);
                    } 
                    documentoServidoresSancionados.append("multa", documentoMulta);
                } 
                
                if (servidorSancionado.getInhabilitacion() != null)
                { 
                    Document documentoInhabilitacion = new Document();
                    if (servidorSancionado.getInhabilitacion().getPlazo() != null)
                    {
                        documentoInhabilitacion.append("plazo", servidorSancionado.getInhabilitacion().getPlazo());
                    }
                    if (servidorSancionado.getInhabilitacion().getFechaInicial() != null)
                    {
                        documentoInhabilitacion.append("fechaInicial", servidorSancionado.getInhabilitacion().getFechaInicial());
                    }
                    if (servidorSancionado.getInhabilitacion().getFechaFinal() != null)
                    {
                        documentoInhabilitacion.append("fechaFinal", servidorSancionado.getInhabilitacion().getFechaFinal());
                    }
                    if (!documentoInhabilitacion.isEmpty())
                    {
                        documentoServidoresSancionados.append("inhabilitacion", documentoInhabilitacion);
                    }
                }
                
                if (servidorSancionado.getObservaciones() != null)
                {
                    documentoServidoresSancionados.append("observaciones", servidorSancionado.getObservaciones());
                }
                
                if (servidorSancionado.getDocumentos() != null)
                { 
                    documentos = new ArrayList<>();
                    for (int i = 0; i < servidorSancionado.getDocumentos().size(); i++)
                    { 
                        Document documentoElementos = new Document();
                        documentoElementos.append("id", servidorSancionado.getDocumentos().get(i).getId());
                        if (servidorSancionado.getDocumentos().get(i).getTipo() != null)
                        {
                            documentoElementos.append("tipo", servidorSancionado.getDocumentos().get(i).getTipo());
                        }
                        documentoElementos.append("titulo", servidorSancionado.getDocumentos().get(i).getTitulo());
                        documentoElementos.append("descripcion", servidorSancionado.getDocumentos().get(i).getDescripcion());
                        documentoElementos.append("url", servidorSancionado.getDocumentos().get(i).getUrl());
                        documentoElementos.append("fecha", servidorSancionado.getDocumentos().get(i).getFecha());
                        documentos.add(documentoElementos);
                    } 
                    documentoServidoresSancionados.append("documentos", documentos);
                } 
                
            } 
            else
            { 
                
                documentoServidoresSancionados.append("fechaCaptura", servidorSancionado.getFechaCaptura());
                
                if (camposPublicosServidoresSancionados.getExpediente().equals("1") && servidorSancionado.getExpediente() != null)
                {
                    documentoServidoresSancionados.append("expediente", servidorSancionado.getExpediente());
                }
                
                if (servidorSancionado.getInstitucionDependencia() == null)
                { 
                    documentoServidoresSancionados.append("institucionDependencia",
                            new Document()
                                    .append("nombre", null));
                } 
                else
                { 
                    Document documentoInstitucionDependencia = new Document();
                    documentoInstitucionDependencia.append("nombre", servidorSancionado.getInstitucionDependencia().getNombre());
                    if (camposPublicosServidoresSancionados.getInstitucionDependencia().getSiglas().equals("1") && servidorSancionado.getInstitucionDependencia().getSiglas() != null)
                    {
                        documentoInstitucionDependencia.append("siglas", servidorSancionado.getInstitucionDependencia().getSiglas());
                    }
                    if (camposPublicosServidoresSancionados.getInstitucionDependencia().getClave().equals("1") && servidorSancionado.getInstitucionDependencia().getClave() != null)
                    {
                        documentoInstitucionDependencia.append("clave", servidorSancionado.getInstitucionDependencia().getClave());
                    }
                    documentoServidoresSancionados.append("institucionDependencia", documentoInstitucionDependencia);
                } 
                
                if (servidorSancionado.getServidorPublicoSancionado() == null)
                { 
                    documentoServidoresSancionados.append("servidorPublicoSancionado",
                            new Document()
                                    .append("nombres", null)
                                    .append("primerApellido", null)
                                    .append("puesto", null)
                    );
                } 
                else
                { 
                    Document documentoServidorPublicoSancionado = new Document();

                    if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getRfc().equals("1") && servidorSancionado.getServidorPublicoSancionado().getRfc() != null)
                    {
                        documentoServidorPublicoSancionado.append("rfc", servidorSancionado.getServidorPublicoSancionado().getRfc());
                    }
                    if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getCurp().equals("1") && servidorSancionado.getServidorPublicoSancionado().getCurp() != null)
                    {
                        documentoServidorPublicoSancionado.append("curp", servidorSancionado.getServidorPublicoSancionado().getCurp());
                    }
                    documentoServidorPublicoSancionado.append("nombres", servidorSancionado.getServidorPublicoSancionado().getNombres());
                    documentoServidorPublicoSancionado.append("primerApellido", servidorSancionado.getServidorPublicoSancionado().getPrimerApellido());
                    if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getSegundoApellido().equals("1") && servidorSancionado.getServidorPublicoSancionado().getSegundoApellido() != null)
                    {
                        documentoServidorPublicoSancionado.append("segundoApellido", servidorSancionado.getServidorPublicoSancionado().getSegundoApellido());
                    }
                    if (servidorSancionado.getServidorPublicoSancionado().getGenero() != null)
                    { 
                        Document documentoGenero = new Document();
                        if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getGenero().getClave().equals("1") && servidorSancionado.getServidorPublicoSancionado().getGenero().getClave() != null)
                        {
                            documentoGenero.append("clave", servidorSancionado.getServidorPublicoSancionado().getGenero().getClave());
                        }
                        if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getGenero().getValor().equals("1") && servidorSancionado.getServidorPublicoSancionado().getGenero().getValor() != null)
                        {
                            documentoGenero.append("valor", servidorSancionado.getServidorPublicoSancionado().getGenero().getValor());
                        }

                        if (!documentoGenero.isEmpty())
                        {
                            documentoServidorPublicoSancionado.append("genero", documentoGenero);
                        }
                    } 
                    documentoServidorPublicoSancionado.append("puesto", servidorSancionado.getServidorPublicoSancionado().getPuesto());
                    if (camposPublicosServidoresSancionados.getServidorPublicoSancionado().getNivel().equals("1") && servidorSancionado.getServidorPublicoSancionado().getNivel() != null)
                    {
                        documentoServidorPublicoSancionado.append("nivel", servidorSancionado.getServidorPublicoSancionado().getNivel());
                    }

                    documentoServidoresSancionados.append("servidorPublicoSancionado", documentoServidorPublicoSancionado);
                } 
                
                if (camposPublicosServidoresSancionados.getAutoridadSancionadora().equals("1") && servidorSancionado.getAutoridadSancionadora() != null)
                {
                    documentoServidoresSancionados.append("autoridadSancionadora", servidorSancionado.getAutoridadSancionadora());
                }
                
                if (servidorSancionado.getTipoFalta() == null)
                { 
                    documentoServidoresSancionados.append("tipoFalta",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null));
                } 
                else
                { 
                    Document documentoTipoFalta = new Document();
                    
                    documentoTipoFalta.append("clave", servidorSancionado.getTipoFalta().getClave());
                    documentoTipoFalta.append("valor", servidorSancionado.getTipoFalta().getValor());
                    
                    if (camposPublicosServidoresSancionados.getTipoFalta().getDescripcion().equals("1") && servidorSancionado.getTipoFalta().getDescripcion() != null)
                    {
                        documentoTipoFalta.append("descripcion", servidorSancionado.getTipoFalta().getDescripcion());
                    }
                    
                    documentoServidoresSancionados.append("tipoFalta", documentoTipoFalta);
                } 
                
                if (servidorSancionado.getTipoSancion() == null)
                { 
                    documentoServidoresSancionados.append("tipoSancion",
                            new Document()
                                    .append("clave", null)
                                    .append("valor", null));
                } 
                else
                { 
                    arregloTipoSancion = new ArrayList<>();
                    for (int j = 0; j < servidorSancionado.getTipoSancion().size(); j++)
                    { 
                        Document documentoTipoSancion = new Document();
                        
                        documentoTipoSancion.append("clave", servidorSancionado.getTipoSancion().get(j).getClave());
                        
                        
                        documentoTipoSancion.append("valor", servidorSancionado.getTipoSancion().get(j).getValor());
                        
                        
                        if (camposPublicosServidoresSancionados.getTipoSancion().get(0).getDescripcion().equals("1") && servidorSancionado.getTipoSancion().get(j).getDescripcion() != null)
                        {
                            documentoTipoSancion.append("descripcion", servidorSancionado.getTipoSancion().get(j).getDescripcion());
                        }
                        
                        arregloTipoSancion.add(documentoTipoSancion);
                    } 
                    documentoServidoresSancionados.append("tipoSancion", arregloTipoSancion);
                } 
                
                documentoServidoresSancionados.append("causaMotivoHechos", camposPublicosServidoresSancionados.getCausaMotivoHechos().equals("1") ? servidorSancionado.getCausaMotivoHechos() : "");
                
                if (servidorSancionado.getResolucion() != null)
                { 
                    Document documentoResolucion = new Document();
                    if (camposPublicosServidoresSancionados.getResolucion().getUrl().equals("1") && servidorSancionado.getResolucion().getUrl() != null)
                    {
                        documentoResolucion.append("url", servidorSancionado.getResolucion().getUrl());
                    }
                    documentoResolucion.append("fechaResolucion", servidorSancionado.getResolucion().getFechaResolucion());
                    documentoServidoresSancionados.append("resolucion", documentoResolucion);
                } 
                
                if (servidorSancionado.getMulta() != null)
                { 
                    Document documentoMulta = new Document();
                    documentoMulta.append("monto", servidorSancionado.getMulta().getMonto());
                    Document documentoMoneda = new Document();
                    if (servidorSancionado.getMulta().getMoneda() != null)
                    { 
                        documentoMoneda.append("clave", servidorSancionado.getMulta().getMoneda().getClave());
                        documentoMoneda.append("valor", servidorSancionado.getMulta().getMoneda().getValor());
                        documentoMulta.append("moneda", documentoMoneda);
                    } 
                    else
                    { 
                        documentoMoneda.append("clave", null);
                        documentoMoneda.append("valor", null);
                        documentoMulta.append("moneda", documentoMoneda);
                    } 
                    documentoServidoresSancionados.append("multa", documentoMulta);
                } 
                
                if (servidorSancionado.getInhabilitacion() != null)
                { 
                    Document documentoInhabilitacion = new Document();
                    if (camposPublicosServidoresSancionados.getInhabilitacion().getPlazo().equals("1") && servidorSancionado.getInhabilitacion().getPlazo() != null)
                    {
                        documentoInhabilitacion.append("plazo", servidorSancionado.getInhabilitacion().getPlazo());
                    }
                    if (camposPublicosServidoresSancionados.getInhabilitacion().getFechaInicial().equals("1") && servidorSancionado.getInhabilitacion().getFechaInicial() != null)
                    {
                        documentoInhabilitacion.append("fechaInicial", servidorSancionado.getInhabilitacion().getFechaInicial());
                    }
                    if (camposPublicosServidoresSancionados.getInhabilitacion().getFechaFinal().equals("1") && servidorSancionado.getInhabilitacion().getFechaFinal() != null)
                    {
                        documentoInhabilitacion.append("fechaFinal", servidorSancionado.getInhabilitacion().getFechaFinal());
                    }
                    if (!documentoInhabilitacion.isEmpty())
                    {
                        documentoServidoresSancionados.append("inhabilitacion", documentoInhabilitacion);
                    }
                } 
                
                if (camposPublicosServidoresSancionados.getObservaciones().equals("1") && servidorSancionado.getObservaciones() != null)
                {
                    documentoServidoresSancionados.append("observaciones", servidorSancionado.getObservaciones());
                }
                
                if (servidorSancionado.getDocumentos() != null)
                { 
                    documentos = new ArrayList<>();
                    for (int i = 0; i < servidorSancionado.getDocumentos().size(); i++)
                    { 
                        Document documentoElementos = new Document();
                        documentoElementos.append("id", servidorSancionado.getDocumentos().get(i).getId());
                        if (servidorSancionado.getDocumentos().get(i).getTipo() != null)
                        {
                            documentoElementos.append("tipo", servidorSancionado.getDocumentos().get(i).getTipo());
                        }
                        documentoElementos.append("titulo", servidorSancionado.getDocumentos().get(i).getTitulo());
                        documentoElementos.append("descripcion", servidorSancionado.getDocumentos().get(i).getDescripcion());
                        documentoElementos.append("url", servidorSancionado.getDocumentos().get(i).getUrl());
                        documentoElementos.append("fecha", servidorSancionado.getDocumentos().get(i).getFecha());
                        documentos.add(documentoElementos);
                    } 
                    documentoServidoresSancionados.append("documentos", documentos);
                }
            } 
            listaDocumentosServidoresSancionados.add(documentoServidoresSancionados);
        } 

        documentoServidoresSancionados = new Document();
        documentoServidoresSancionados.append("pagination",
                new Document("pageSize", atributosDePaginacion.getPageSize())
                        .append("page", atributosDePaginacion.getPage())
                        .append("totalRows", atributosDePaginacion.getTotalRows())
                        .append("hasNextPage", atributosDePaginacion.getHasNextPage())
        );
        
        documentoServidoresSancionados.append("results", listaDocumentosServidoresSancionados);
        return documentoServidoresSancionados.toJson();
    }

    /**
     * Obtener dependencias servidores sancionados
     *
     * @return Dependencias servidores sancionados
     *
     * @throws Exception
     */
    @Override
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresSancionados() throws Exception
    {
        return DAOWs.obtenerDependenciasServidoresSancionados();
    }

    /**
     * Generar en formato Json dependencias servidores sancionados
     *
     * @param dependenciasServidoresSancionados Dependencias de servidores
     *                                          sancionados
     *
     * @return Formato Json dependencias servidores sancionados
     *
     * @throws Exception
     */
    @Override
    public String generarJsonDependenciasServidoresSancionados(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasServidoresSancionados) throws Exception
    {

        List<Document> listaDocumentosDependencias = new ArrayList<>(); 
        Document documentoDependencia;                                          

        for (INSTITUCION_DEPENDENCIA dependencia : dependenciasServidoresSancionados)
        { 

            documentoDependencia = new Document();
            documentoDependencia.append("nombre", dependencia.getNombre());
            documentoDependencia.append("siglas", dependencia.getSiglas());
            documentoDependencia.append("clave", dependencia.getClave());
            listaDocumentosDependencias.add(documentoDependencia);
        } 

        return new Gson().toJson(listaDocumentosDependencias);
    }

    
    /**
     * Convierte una cadena, en una expresión regular que contiene o no, acentos
     *
     * @param palabra
     *
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    {

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        { 
            switch (letra)
            {
                
                case 'a':
                    expReg += "[aáAÁ]";
                    break;
                case 'á':
                    expReg += "[aáAÁ]";
                    break;
                case 'e':
                    expReg += "[eéEÉ]";
                    break;
                case 'é':
                    expReg += "[eéEÉ]";
                    break;
                case 'i':
                    expReg += "[iíIÍ]";
                    break;
                case 'í':
                    expReg += "[iíIÍ]";
                    break;
                case 'o':
                    expReg += "[oóOÓ]";
                    break;
                case 'ó':
                    expReg += "[oóOÓ]";
                    break;
                case 'u':
                    expReg += "[uúUÚ]";
                    break;
                case 'ú':
                    expReg += "[uúUÚ]";
                    break;
                default:
                    expReg += letra;
                    break;
            }
        } 
        return expReg;
    }

    /**
     * Valida RFC
     *
     * @param rfc RFC
     *
     * @return Falso o verdadero
     *
     * @throws Exception
     */
    @Override
    public boolean esValidoRFC(String rfc) throws Exception
    {
        return DAOWs.esValidoRFC(rfc);
    }

}