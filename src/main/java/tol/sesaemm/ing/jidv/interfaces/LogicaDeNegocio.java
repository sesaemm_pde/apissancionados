/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s3ssancionados.FILTRO_SSANCIONADOS_WS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOSROOT;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */

public interface LogicaDeNegocio
{
    
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion ,String strEstatusConsulta) throws Exception;
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception;
    public FILTRO_SSANCIONADOS_WS obtenerValoresPorDefaultFiltroServidoresSancionados (FILTRO_SSANCIONADOS_WS estructuraPost) throws Exception;
    public SSANCIONADOSROOT obtenerServidoresPublicosSancionadosPost(FILTRO_SSANCIONADOS_WS estructuraPost) throws Exception;
    public SSANCIONADOS obtenerEstatusDePrivacidadCamposServidoresSancionados() throws Exception;
    public String generarJsonServidoresSancionados(SSANCIONADOSROOT servidoresSancionados, SSANCIONADOS camposPublicosServidoresSancionados, String rfcSolicitante) throws Exception;
    public ArrayList<INSTITUCION_DEPENDENCIA> obtenerDependenciasServidoresSancionados() throws Exception;
    public String generarJsonDependenciasServidoresSancionados(ArrayList<INSTITUCION_DEPENDENCIA> dependenciasServidoresSancionados) throws Exception;
    public String crearExpresionRegularDePalabra(String palabra);
    public boolean esValidoRFC(String rfc) throws Exception;    
    
}